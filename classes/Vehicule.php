<?php


class Vehicule
{
    public $masse;
    public $vitesse;
    public $dimensions; //array()
    public $rayon = 15;

    public function calculerEnergieCinetique()
    {
        if ($this->masse > 0)
            return 0.5 * $this->masse * ($this->vitesse ** 2);

        else
            return false;

    }

    public function calculerForceCentrifuge()
    {
        if ($this->masse > 0)
            return ((0.5 * $this->masse * ($this->vitesse ** 2)) / $this->rayon);

        else
            return false;

    }

    public function getMasse()
    {
        return $this->masse;
    }

    public function setMasse(float $m)
    {
        $this->masse = $m;
    }

    public function getVitesse()
    {
        return $this->vitesse;
    }

    public function setVitesse(float $v)
    {
        $this->vitesse = $v;
    }

    public function getRayon()
    {
        return $this->rayon;
    }

    public function setRayon(float $r)
    {
        $this->rayon = $r;
    }

}
